<?php
    session_start();
    if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
    {
        header("Location: login.php?message=nologin");
    }

    include "../config/database_uts.php";

    $rs = $db->query("SELECT * FROM user");

    // $rs->setFetchMode(PDO::FETCH_ASSOC);
    $rs->setFetchMode(PDO::FETCH_OBJ);
?>
    <table border=1 cellspacing=0 cellpadding=0>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Telepon</th>
            <th>Peran</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
<?php
    $i = 1;
    while($data = $rs->fetch())
    {
?>
    <tr>
        <td><?php echo $i?></td>
        <td><?php echo $data->nama?></td>
        <td><?php echo $data->email?></td>
        <td><?php echo $data->telp?></td>
        <td><?php echo $data->peran?></td>
        <td><?php echo $data->active==0?"Non Aktif":"Aktif"?></td>
        <td><a href="edit.php">Edit</a> | <a href="delete.php">Delete</a></td>
    </tr>
<?php
        $i++;
    }
?>
    </table>