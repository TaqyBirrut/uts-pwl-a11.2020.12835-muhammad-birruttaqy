<?php
    include "../config/database_uts.php";

    $uname = $_POST['nama'];
    $passwd = $_POST['password'];
    $cpasswd = $_POST['cpasswd'];
    $email = $_POST['email'];
    $telepon = $_POST['telp'];
    $peran = $_POST['peran'];
    $id_dari_form = $_POST['id'];

    if($passwd==$cpasswd)
    {
    	$id = explode("|",base64_decode($id_dari_form));

        $psw = password_hash($passwd,PASSWORD_DEFAULT);

	    $upd = $db->prepare("UPDATE user SET nama=?,password=?,email=?,telp=?,peran=? WHERE id=?");
	    $upd->execute([$uname,$psw,,$email,$telepon,$peran,$id[1]]);
        header("Location: view_data.php?message=success");  	
    }
    else
    {
        header("Location: edit.php?message=not-match");
    }
?>