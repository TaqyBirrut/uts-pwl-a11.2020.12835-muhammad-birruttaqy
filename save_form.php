<?php
    include "../config/database_uts.php";

    $uname = $_POST['nama'];
    $passwd = $_POST['password'];
    $cpasswd = $_POST['cpasswd'];
    $email = $_POST['email'];
    $telepon = $_POST['telp'];
    $peran = $_POST['peran'];

    if($passwd==$cpasswd)
    {
        $psw = password_hash($passwd,PASSWORD_DEFAULT);

        // $db->query("INSERT INTO users (username,passwd) VALUES ('".$uname."','".$psw."')")

	    $ins = $db->prepare("INSERT INTO user (nama,password,email,telp,peran) VALUES (?,?,?,?,?)");
        $ins->execute([$uname,$psw,$email,$telepon,$peran]);
        header("Location: registration.php?msg=success");  	
    }
    else
    {
        header("Location: registration.php?msg=not-match");
    }
?>